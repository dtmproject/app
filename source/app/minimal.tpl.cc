/**
 * @file minimal.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-02-26, UK
 *
 * @brief DTM++/APP: A minimal FEM problem for deal.II.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/APP.                                           */
/*                                                                            */
/*  DTM++/APP is free software: you can redistribute it and/or modify         */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/APP is distributed in the hope that it will be useful,              */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/APP.   If not, see <http://www.gnu.org/licenses/>.       */

/* ---------------------------------------------------------------------
 *
 * Copyright (C) 1999 - 2014 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE at
 * the top level of the deal.II distribution.
 *
 * ---------------------------------------------------------------------
 */

// PROJECT includes
#include <app/minimal.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>

#include <deal.II/base/point.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/tria.h>

#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/fe/fe.h>
#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/mapping.h>
#include <deal.II/fe/mapping_q.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/lac/vector.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/data_component_interpretation.h>

// C++ includes
#include <iostream>
#include <fstream>

namespace DTM {
namespace app {

	template<int dim>
	Minimal<dim>::Minimal() {
	}


	template<int dim>
	Minimal<dim>::~Minimal() {
	}


	template<int dim>
	void
	Minimal<dim>::
	run () {
		dealii::Triangulation<dim> tria;
		dealii::GridGenerator::hyper_cube(tria);
		
		// minimal fe/dof
		dealii::FE_Q<dim> fe(1);
		dealii::DoFHandler<dim> dof(tria);
		dof.distribute_dofs(fe);
		
		const dealii::types::global_dof_index n_dofs(dof.n_dofs());
		
		dealii::Vector<double> solution_vector(n_dofs);
		solution_vector(1) = 10.;
		
		
		std::vector<std::string> data_field_names;
		data_field_names.push_back("test");
		
		std::vector<dealii::DataComponentInterpretation::DataComponentInterpretation> dci_field;
		dci_field.push_back(dealii::DataComponentInterpretation::component_is_scalar);
		
		// create dealii::DataOut object and attach the data
		dealii::DataOut<dim> data_out;
		data_out.attach_dof_handler(dof);
		
		data_out.add_data_vector(
			solution_vector,
			data_field_names,
			dealii::DataOut<dim>::type_dof_data,
			dci_field
		);
		data_out.build_patches(1);
		
		// prepare output filenames and output stream
		std::ostringstream stream;
		stream.str("");
		stream << "solution_dim_" << dim << ".vtu";
		std::string data_filename = stream.str();
		std::ofstream output_file_stream(data_filename.c_str());
		
		data_out.write_vtu(
			output_file_stream
		);
		
		output_file_stream.close();
	}

}} // namespaces

// Instanciation includes
#include "minimal.inst.in"
