/**
 * @file minimal.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-02-26, UK
 *
 * @brief DTM++/APP: A minimal FEM problem for deal.II.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/APP.                                           */
/*                                                                            */
/*  DTM++/APP is free software: you can redistribute it and/or modify         */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/APP is distributed in the hope that it will be useful,              */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/APP.   If not, see <http://www.gnu.org/licenses/>.       */

/* ---------------------------------------------------------------------
 *
 * Copyright (C) 1999 - 2014 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE at
 * the top level of the deal.II distribution.
 *
 * ---------------------------------------------------------------------
 */

#ifndef minimal_tpl_hh
#define minimal_tpl_hh

namespace DTM {
namespace app {

	template<int dim>
	class Minimal {
	public:
		Minimal();
		
		virtual ~Minimal();
		
		virtual void run();
	};

}} // namespaces

#endif
